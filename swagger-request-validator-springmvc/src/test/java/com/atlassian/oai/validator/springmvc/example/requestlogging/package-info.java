/**
 * This example extends the {@link com.atlassian.oai.validator.springmvc.example.simple} example by an
 * request logging interceptor, which will log the incoming request including its body.
 *
 * @see com.atlassian.oai.validator.springmvc.example.requestlogging.RestRequestLoggingValidationConfig
 * @see com.atlassian.oai.validator.springmvc.example.requestlogging.RestRequestLoggingValidationConfig.RequestLoggingInterceptor
 */
package com.atlassian.oai.validator.springmvc.example.requestlogging;